// ==UserScript==
// @name         Point of Rental Essential Improvements
// @namespace    http://tampermonkey.net/
// @version      0.4
// @description  Improve usability, layout, and features of Point of Rental website
// @author       Shane Burgess
// @match        https://pointofrentalcloud.com/*
// @grant        none
// @noframes
// @require      file://C:\Users\shane\Documents\BitBucket\BWLScriptsAndTools\PointOfRental\Tampermonkey\script_execution_mgr.js
// @require      file://C:\Users\shane\Documents\BitBucket\BWLScriptsAndTools\PointOfRental\Tampermonkey\por_common.js
// @require      file://C:\Users\shane\Documents\BitBucket\BWLScriptsAndTools\PointOfRental\Tampermonkey\por_safety_cover.js
// @require      file://C:\Users\shane\Documents\BitBucket\BWLScriptsAndTools\PointOfRental\Tampermonkey\por_all_pages.js
// @require      file://C:\Users\shane\Documents\BitBucket\BWLScriptsAndTools\PointOfRental\Tampermonkey\por_p_inventory.js
// @require      file://C:\Users\shane\Documents\BitBucket\BWLScriptsAndTools\PointOfRental\Tampermonkey\por_p_item.js
// @require      file://C:\Users\shane\Documents\BitBucket\BWLScriptsAndTools\PointOfRental\Tampermonkey\por_p_contract.js
// @require      file://C:\Users\shane\Documents\BitBucket\BWLScriptsAndTools\PointOfRental\Tampermonkey\por_p_printouts.js
// ==/UserScript==

// https://stackoverflow.com/questions/49509874/how-to-update-tampermonkey-script-to-a-local-file-programmatically
(function() {
  'use strict';

  console.log('---Point of Rental Everything!---');
})();