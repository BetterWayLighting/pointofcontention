## This is a public repo, so anybody on the internet can see it. ##
## Do not submit any business-specific code to this repo! ##

# README #

PointOfContention is a collection of Javascript and CSS customizations for the "Rental Essentials" rental management software at PointOfRental.com. Since we don't have control over the code on Point of Rental's servers, all of these changes are "client-side", that is to say, Point of Rental sends the official webpage to your web browser, and then PointOfContention modifies the webpage within your browser tab each time the page is loaded.

### How to use ###

To use these scripts, you will need two browser extensions to run the scripts every time a page from pointofrentalcloud.com is loaded. One is responsible for UserScript (Javascript) injection, and the other is responsible for UserStyle (CSS) injection. For Google Chrome, I recommend [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo) for the Javascript and [Stylus](https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne) for the CSS styling.

#### For the CSS styling ####
[pointofrental_styles.css](../../raw/master/css/pointofrental_styles.user.css) includes styles that directly improve the look and structure of Rental Essentials, and is useful even without Tampermonkey and our custom Javascript.  
[pointofcontention_styles.css](../../raw/master/css/pointofcontention_styles.user.css) includes styles that are required by the Tampermonkey script. It is expected that pointofrental_styles.css is included in addition to pointofcontention_styles.css.

#### For the Javascript ####
After Tampermonkey is installed, go to the [PointOfContention.user.js direct link](../../raw/master/PointOfContention.user.js) and click the Install button.

**TODO:** Describe how to configure tampermonkey to update more regularly and to update external scripts.
Describe how to install the userscript and userstyles after I do it once to get all the details figured out.

### How to verify the scripts are running ###
![Rental Essentials logo with flower and version number](img/RE_flower.png "Rental Essentials logo with flower and version number")

If the **CSS** is running properly, you will see a **flower icon** superimposed over the RE logo, which rotates when you hover over it.  
If the **Javascript** is running properly, you will see a **version number** superimposed over the RE logo. Note that this is Point of Rental's version number, not PointOfContention's version number.  
You may also press F12 to open the Developer Console to see if any exceptions in the scripts are being thrown. On a full page refresh, the last initialization script to run writes ***--- This is a Point of Contention ---*** to the console.

### Development Setup ###

If you'd like to edit these scripts, to fix problems or add new features, you'll want to temporarily disable the online scripts in Tampermonkey and Stylish, and create copies for yourself to edit.

CSS is very convenient to edit directly in the Stylish editor, as the target pages will update automatically with new styles as soon as you save them in the web browser.

To run and edit a local copy of the Tampermonkey scripts, see this StackOverflow post on [How to make a Tampermonkey script @require local files](https://stackoverflow.com/questions/49509874/how-to-update-tampermonkey-script-to-a-local-file-programmatically). With your system properly configured, you should be able to edit any Tampermonkey script (aside from the root one in the Tampermonkey web interface) in your editor of choice, save your changes, and refresh the webpage to have them applied.

You will have to make a copy of the root Tampermonkey script ([PointOfContention.user.js](PointOfContention.user.js)) in the Tampermonkey web interface, and change all of the @require tags to point at the files on your local machine. If you add a new .js file, make sure to add a new @require tag for it in both your private copy of the root script and the repository copy of the root script [PointOfContention.user.js](PointOfContention.user.js).

For users to receive updates to the Tampermonkey script, the @version tag must be updated.