// ==UserScript==
// @name         Point Of Contention
// @namespace    https://bitbucket.org/BetterWayLighting/pointofcontention/
// @version      0.8
// @description  PointOfContention is a collection of Javascript and CSS customizations for the "Rental Essentials" rental management software at PointOfRental.com
// @homepageURL  https://bitbucket.org/BetterWayLighting/pointofcontention/src/master
// @author       Shane Burgess
// @match        https://pointofrentalcloud.com/*
// @grant        none
// @noframes
// @require      https://bitbucket.org/BetterWayLighting/pointofcontention/raw/master/js/script_execution_mgr.js
// @require      https://bitbucket.org/BetterWayLighting/pointofcontention/raw/master/js/por_common.js
// @require      https://bitbucket.org/BetterWayLighting/pointofcontention/raw/master/js/por_safety_cover.js
// @require      https://bitbucket.org/BetterWayLighting/pointofcontention/raw/master/js/por_all_pages.js
// @require      https://bitbucket.org/BetterWayLighting/pointofcontention/raw/master/js/por_p_inventory.js
// @require      https://bitbucket.org/BetterWayLighting/pointofcontention/raw/master/js/por_p_item.js
// @require      https://bitbucket.org/BetterWayLighting/pointofcontention/raw/master/js/por_p_contract.js
// @require      https://bitbucket.org/BetterWayLighting/pointofcontention/raw/master/js/por_p_printouts.js
// ==/UserScript==

// This is the root Tampermonkey script for PointOfContention. All code is
// included via the @require tags above, to allow for easy development.
// https://stackoverflow.com/questions/49509874/how-to-update-tampermonkey-script-to-a-local-file-programmatically
(function() {
  'use strict';

  // This is the last command to run during the initialization phase of all the above @require scripts.
  // Everything that runs after this is a result of event listeners or setTimeout/setInterval events.
  console.log('--- This is a Point of Contention ---');
})();